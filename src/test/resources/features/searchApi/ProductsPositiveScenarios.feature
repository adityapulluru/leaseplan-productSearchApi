@feature:productSearchApiPositiveScanrios
Feature: Verify Positive Scenarios of Search Product API

@positiveScenarios @searchapi
Scenario Outline: Verify Status code of the success response
Given I have product category to search "<productcategory>"
When I call the end point with GET method
Then I verify success response with status code <statuscode>

Examples:
|productcategory|statuscode|
|apple|200|
|orange|200|
|pasta|200|
|cola|200|


@positiveScenarios @searchapi
Scenario Outline:  Verify response contains the products that are same as requested
Given I have product category to search "<productcategory>"
When I call the end point with GET method
Then I verify that response contains the same product as requested

Examples:
|productcategory|
|apple|
|orange|
|pasta|
|cola|


@positiveScenarios  @searchapi
Scenario Outline: Verify response schema
Given I have product category to search "<productcategory>"
When I call the end point with GET method
Then I verify that response schema matches with actual schema "<reference>"

Examples:
|productcategory|reference|
|apple|SuccessRespJsonschema.json|
|orange|SuccessRespJsonschema.json|
|pasta|SuccessRespJsonschema.json|
|cola|SuccessRespJsonschema.json|


@positiveScenarios @searchapi
Scenario Outline: Verify response Headers are valid
Given I have product category to search "<productcategory>"
When I call the end point with GET method
Then I verify that response headers are valid

Examples:
|productcategory|
|apple|
|orange|
|pasta|
|cola|


@positiveScenarios @searchapi
Scenario Outline: Verify response time
Given I have product category to search "<productcategory>"
When I call the end point with GET method
Then I verify that response time is less than <duration> seconds

Examples:
|productcategory|duration|
|apple|5|
|orange|5|
|pasta|5|
|cola|5|


@positiveScenarios @searchapi
Scenario Outline: Verify mandatory fields are non empty in the Response
Given I have product category to search "<productcategory>"
When I call the end point with GET method
Then I verify that following mandatory fields are non-empty
|provider|
|title|
|url|
|price|
|image|
Examples:
|productcategory|
|apple|
|orange|
|pasta|
|cola|







  
