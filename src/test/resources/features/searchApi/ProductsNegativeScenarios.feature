@feature:productSearchApiNegativeScenarios
Feature: Verify Negative Scenarios of Search Product API

@negativeScenarios @searchapi
Scenario Outline: Verify an Error response is recieved when searched for invalid product  
Given I have product category to search "<productcategory>"
When I call the end point with GET method
Then I verify that Error response is recieved
And I verify that product results are not displayed for the invalid product category
And I verify status code of the response is <statuscode>

Examples:
|productcategory|statuscode|
|car|404|
|1234|404|
|car123|404|
|@%#!|404|


@negativeScenarios @searchapi
Scenario Outline:Verify that recieved Error response is for requested item
Given I have product category to search "<productcategory>"
When I call the end point with GET method
Then I verify that Error response recieved is for requested item

Examples:
|productcategory|
|car|
|bike|
|@123%#pen|


@negativeScenarios @searchapi
Scenario Outline:Verify Error response Json schema
Given I have product category to search "<productcategory>"
When I call the end point with GET method
Then I verify Error response Json schema "<reference>"

Examples:
|productcategory|reference|
|car|ErrorRespSchema.json|


@negativeScenarios @searchapi
Scenario Outline:Verify api sends an 405(Method not allowed) status code when user tries to send an invalid HTTP method
Given I have product category to search "<productcategory>"
When I call the Endpoint with invalid HTTP method "<method>"
Then I verify that status code of Error response is <statuscode>

Examples:
|productcategory|method|statuscode|
|apple|POST|405|
|apple|PUT|405|
|apple|DELETE|405|


@negativeScenarios @searchapi
Scenario Outline:Verify Error response when user calls an invalid endpoint
Given I have product category to search "<productcategory>"
When I call an invalid end point
Then I verify that status code of Error response is <statuscode>

Examples:
|productcategory|statuscode|
|apple|404|

