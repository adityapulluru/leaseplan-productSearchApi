package searchApi.actions;

import java.io.File;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import searchApi.resources.Specifications;
import searchApi.resources.Utilities;
import io.restassured.response.*;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.path.json.JsonPath;
import static org.hamcrest.Matchers.*;


public class BaseProductActions extends Utilities{ 
	
	    protected static ValidatableResponse response;
	    protected static Response responseRaw;
	    protected static int jsonSize;
	    protected static String CONTENT_TYPE="application/json";
	    protected static String JSON_CONTENT_TYPE="Content-Type";
	    protected static String JSON_CONTENT_LENGTH="Content-Length";
		
	    
	    /*this is a reusable method to call End point*/
	    
		@Step("Call the End point")
		public void call_end_point(String productcatogory,String method,String bPath) {
		
			Specifications specifications=new Specifications();
			RequestSpecification reqSpecification=specifications.reqspecification(bPath);
			ResponseSpecification resSpecification=specifications.getResponseSpec();
			
			if(method.equals("GET")) {			
				responseRaw=SerenityRest.rest().given().spec(reqSpecification).get(productcatogory);
				response=responseRaw.then().spec(resSpecification);
			 }
			else if(method.equals("POST")){
				response=SerenityRest.rest().given().spec(reqSpecification).post(productcatogory).then().spec(resSpecification);
			}
			else if(method.equals("PUT")){
				response=SerenityRest.rest().given().spec(reqSpecification).put(productcatogory).then().spec(resSpecification);
			}
			else if(method.equals("DELETE")){
				response=SerenityRest.rest().given().spec(reqSpecification).delete(productcatogory).then().spec(resSpecification);
			}
			else {				
				Utilities.generateLogs("Invalid Method");				
			}			
		}
	
		
		
	    /*this is a reusable method to verify status code of the response*/	
		
		@Step("Verify Status {0}")
		public void verify_statuscode(int status) {				
				response.assertThat().statusCode(status);				
	   }
		
		
		
		/*this is a reusable method to verify json schema of the response*/
		
		@Step("Verify Json Schema")
		public void verify_json_schema(String ref) {				
				response.assertThat().body(JsonSchemaValidator.matchesJsonSchema(new File(ref)));				
		}
		
		
		
		/*this is a reusable method to verify response header*/
		
		@Step("Verify response Header")
		public void verifyheader() {		
				response.assertThat().header(JSON_CONTENT_TYPE,CONTENT_TYPE );
				response.assertThat().header(JSON_CONTENT_LENGTH,Integer::parseInt, greaterThan(0));		
		}
		

		
		/*this is a reusable method to verify Error response contains product catogory*/
		
		@Step("Verify Error response recieved is for the Requested Item")
		public void  verifyreqitem(String reqItemActual) {			
				response.assertThat().body("detail.requested_item", equalTo(reqItemActual));		
		}
		

	
		/*this is a reusable method to get the count of the total products 
		in search, which will be used in other methods*/
		
		@Step("Verify count of the products recieved is greater than Zero")
		public void verifycount() {
				jsonSize=responseRaw.jsonPath().getList("").size();					
				Specifications.AssertCondition(jsonSize>0, "Count of the products in response is zero");				
		}
	
	
	
		/*this is a reusable method, to verify title in each and every record
		  in the response*/
		
		@Step("Verify recieved response contains product that is searched")
		public void  verifyCorResponse(String productcatogory) {
			
	 	 		verifycount();	
	 	 		for(int iterator=0;iterator<jsonSize;iterator++) {
		
	 	 			StringBuilder stringbuilder=new StringBuilder();					
	 	 			String path=stringbuilder.append("[").append(iterator).append("]").append(".title").toString();				
	 	 			response.assertThat().body(path,containsStringIgnoringCase(productcatogory));
					
		 	 	}
	
		}
	
}