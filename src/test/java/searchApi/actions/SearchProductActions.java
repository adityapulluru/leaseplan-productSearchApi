package searchApi.actions;

import java.util.List;
import java.util.concurrent.TimeUnit;
import net.thucydides.core.annotations.Step;
import searchApi.resources.Utilities;
import static org.hamcrest.Matchers.*;

public class SearchProductActions extends BaseProductActions {
	
	   protected static String 	ERR_RESPONSE_MESSAGE = "detail.message";
	   protected static String  ERR_RESPONSE_ERROR = "detail.error";
	   protected static String  ERR_TITLE_PATH = "[0].titile";
	   protected static String  ERR_PROVIDER_PATH = "[0].provider";
		
	   
	   
		/*This method is to verify received error response contains fields 
		like message, error which specifies received error response is valid one*/		
		@Step("Verify recieved Error response is valid")
		public void verifyErrorResp() {
			
				response.assertThat().body(ERR_RESPONSE_MESSAGE, equalTo("Not found"));
				response.assertThat().body(ERR_RESPONSE_ERROR, equalTo(true));
		
		}
		
		
		
		/*This method id to verify error response does not contain fields 
		like Title, Provider which are available for valid response*/ 		
		@Step("Verify recieved response is an Error response")
		public void ver_results() {	
				
			try {
					
				response.assertThat().body(ERR_TITLE_PATH,equalTo(null));
				response.assertThat().body(ERR_PROVIDER_PATH,equalTo(null));
				
			}			
			catch(Exception e) {
					
				Utilities.generateLogs("Valid response");	
						
			}			
		}
		
		
		
		/*This method is to verify response time is in permitted limits	*/		
		@Step("Verify Response time to be less than {0}")
		public void verifyResTime(int duration){
			
				response.assertThat().time(lessThan((long)duration),TimeUnit.SECONDS);
			
		}
		
		
		
		/*This method is to verify that all mandatory fields are 
		present in the response */		
		@Step("Verify mandatory fields are non null {0} in the response")
		public void verifymandatoryfields(List<String> fields) {
			
				verifycount();		
				for(int iterator=0;iterator<jsonSize;iterator++) {
				
					for(String mandatoryField:fields) {				
						StringBuilder stringbuilder=new StringBuilder();
						String path=stringbuilder.append("[").append(iterator).append("].").append(mandatoryField).toString();
						response.assertThat().body(path,not(""));			
					}			
				}
			
			}

}
