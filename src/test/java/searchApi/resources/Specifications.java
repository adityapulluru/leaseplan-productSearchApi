package searchApi.resources;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.junit.Assert;

public class Specifications extends Utilities{
	
	 protected static String BASE_URL="baseUrl";
	
	
	//This method is to generate Request specification
	public RequestSpecification reqspecification(String path) {	
		
		RequestSpecBuilder reqBuilder = new RequestSpecBuilder();
		reqBuilder.setBaseUri(fetchPropertyVariables(BASE_URL));
		reqBuilder.setBasePath(fetchPropertyVariables(path));	
		RequestSpecification reqSpec = reqBuilder.build();	
		return reqSpec;		
	}
	
	
	
	//This method is to generate response specification
	public ResponseSpecification getResponseSpec(){		
		ResponseSpecBuilder respec = new ResponseSpecBuilder();
		ResponseSpecification responseSpecification = respec.build();
		return responseSpecification;	
	}
	
	
	
	public static void AssertStrings(String actual,String expected,String message) {		
		Assert.assertEquals(message, expected, actual);
	}
	
	
	
	public static void AssertIntegers(int actual,int expected,String message) {		
		Assert.assertEquals(message, expected, actual);		
	}
	
	
	
	public static void AssertCondition(Boolean condition,String message) {		
		Assert.assertTrue(message, condition);		
	}
	
	
	
}
