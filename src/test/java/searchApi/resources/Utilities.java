package searchApi.resources;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import net.thucydides.core.guice.Injectors;
import net.thucydides.core.util.EnvironmentVariables;
import static net.serenitybdd.core.environment.EnvironmentSpecificConfiguration.from;

public class Utilities {
	
	private static Logger LOGGER = LoggerFactory.getLogger(Utilities.class);
	
	
	//this method is to fetch property variables from Serenity Config file
	public static String fetchPropertyVariables(String url) {
		
        EnvironmentVariables environmentVariables = Injectors.getInjector()
                .getInstance(EnvironmentVariables.class);
        
        String envPropertyUrl=null;
        
        switch (url){
        
	        	case "baseUrl"        :	envPropertyUrl = from(environmentVariables).getProperty("baseurl");
	        							break;
	        					
	        	case "basepath"		  : envPropertyUrl = from(environmentVariables).getProperty("basepath");
	        					 		break;     						
							
	        	case "invalidbasepath":	envPropertyUrl = from(environmentVariables).getProperty("invalidbasepath");
										break;			      		
        }
        
        return envPropertyUrl;
        
	}
	
	
	//This method is to generate logs while execution
	public static void generateLogs(String msg){
		
		 LOGGER.info(msg);
	}
	
	
	
	public void asserttrue(String successmsg,String failedmsg,String expected,String actual) {
		
		     Assert.assertEquals(failedmsg,expected,actual);		
	}
	
	
}
