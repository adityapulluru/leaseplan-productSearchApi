package searchApi.stepdefinitions;

import java.util.List;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;
import searchApi.actions.SearchProductActions;

public class SearchStepDefinitions {
	
	@Steps
	public SearchProductActions searchProduct;
	
	 @Then("I verify that Error response is recieved")
	   public void user_recieves_error_response(){
		searchProduct.verifyErrorResp();
	   }
	   
	 @And("I verify that product results are not displayed for the invalid product category")
	   public void verify_results() {
		   searchProduct.ver_results();
	   }
	   
	  
	 @Then("I verify that response time is less than {int} seconds")
	    public void verifyResponseTime(int duration) {
	    	searchProduct.verifyResTime(duration);
	    	
	    }
	 	   
	 @Then("I verify that following mandatory fields are non-empty")
	   public void verifymandatoryfieldsarenonempty(DataTable mandatorydflds) {
		   
		   List<String> filds=mandatorydflds.asList();
		   searchProduct.verifymandatoryfields(filds);
	   }
	
}




