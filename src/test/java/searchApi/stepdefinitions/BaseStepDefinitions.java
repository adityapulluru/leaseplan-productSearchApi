package searchApi.stepdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
//import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import searchApi.actions.BaseProductActions;
import searchApi.actions.SearchProductActions;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.contains;

import java.util.List;

public class BaseStepDefinitions {

	protected static String VALID_BASE_PATH="basepath";
	protected static String INVALID_BASE_PATH="invalidbasepath";
	
    @Steps
    public BaseProductActions baseProduct;
 
    public static String productCatogory;
    
    @Given("I have product category to search {string}")
    public void search_product_catogory(String catogory) {
    	productCatogory=catogory;	
    }
    
    
    @When("I call the end point with GET method")
    public void heCallsEndpoint() {	
    	baseProduct.call_end_point(productCatogory,"GET",VALID_BASE_PATH);
    }
    
    
    @Then("I verify success response with status code {int}")
    public void verifysuccessresponse(int statusCode) {	
    	baseProduct.verify_statuscode(statusCode);
    }
    
    
    @Then("I verify that response schema matches with actual schema {string}")
    public void verifyschema(String schemaref) {
    	baseProduct.verify_json_schema(schemaref);
    }
    
    
    @Then("I verify that response headers are valid")
    public void verifyheaders() {
    	baseProduct.verifyheader();	
    }
 

    @And("I verify status code of the response is {int}")
    public void verify_status_code(int statusCode) {
	   baseProduct.verify_statuscode(statusCode);
    }
   
    
    @Then("I verify that Error response recieved is for requested item")
    public void user_recieves_correct_error_response() {   
	   baseProduct.verifyreqitem(productCatogory);
    }
   
    
   @Then("I verify Error response Json schema {string}")
   public void verify_error_response_json_schema(String referenceSchema) {
	   baseProduct.verify_json_schema(referenceSchema);
   }
   
   
   @When("I call the Endpoint with invalid HTTP method {string}")
   public void callendpointwithinvalidmethod(String method) {
	   baseProduct.call_end_point(productCatogory,method,VALID_BASE_PATH);
   }
   
   
   @Then("I verify that status code of Error response is {int}")
   public void verifyerrcode(int statusCode) {
	   baseProduct.verify_statuscode(statusCode);
   }
   
   
   @When("I call an invalid end point")
   public void callinvalidendpoint() {
	   baseProduct.call_end_point(productCatogory,"GET",INVALID_BASE_PATH);
   }
   
   
   @Then("I verify that response contains the same product as requested")
   public void verifyCorrectResponse() {
   		   baseProduct.verifyCorResponse(productCatogory);
   }

    
}
