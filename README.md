**LeasePlan-BE QA AUTOMATION TEST ASSIGNMENT**

**Introduction:**

This assignment is an API Automation solution to automate End Point: https://waarkoop-server.herokuapp.com.

This project has been built with a combination of Java, Serenity BDD, Rest Assured and Maven framework.

**Context:**

In this project user calls API for different products and verify whether the results are valid for valid product categories and verifies error msgs for invalid products.


**Requirements:**

Below dependencies needs to be installed

Java 8 or higher,Maven,IDE

To Download the Project:

->User can download the project from https://gitlab.com/adityapulluru/leaseplan-productSearchApi.git

or

->Can import directly from IDE as projects from GIT from url- https://gitlab.com/adityapulluru/leaseplan-productSearchApi.git


**Project Setup:**

.User Imports the project and setups the project in IDE

**Feature file:**

.Feature files are located under the folder->/src/test/resources/features

.For Positive and Negative scenarios two separate feature files are created.

.There are 6 positive scenarios and 5 negative scenarios has been implemented.

.Feature file has been written in Gherkin Language with tags @positive,@negative and @searchapi

.user can add new scenarios/Tests, user has to create a new  .feature file under /src/test/resources/features folder only.

.user is suppose to write scenarios in Gherkin language

.After writing scenarios, add step definition file at->/src/test/java/searchApi/stepdefinitions
and implementation of the steps to be written in java language under the folder->/src/test/java/searchApi/actions

**Step Definition:**

.Step Definitions are located files are located under the folder->/src/test/java/searchApi/stepdefinitions

.Common step definations which can be re used are placed under the folder->/src/test/java/searchApi/stepdefinitions/BaseStepDefinitions.java

.it is always good to create functionality specific step definition files

**Action files:**

.Action files/Java files are placed under the folder->/src/test/java/searchApi/actions

.All the coding related logics are written here.

.Methods that can be reused are placed under class file->/src/test/java/searchApi/actions/BaseProductActions.java

**Resource files:**

.Project level Re usable methods and utilities are placed here, under the path->/src/test/java/searchApiresources

.Execution starts at Runner file located->/src/test/java/searchApi/resources/TestRunner.java
.Url's has been saved in Serenity.config file for better visibility and has been called in the methods.


**Execution:**

After project has been setup and made sure there are no compilation errors.

**In Maven:**

1.Open command promt pointing to the location pom.xml

2.provide the command "mvn clean verify"

3.After the run, results are generated with Serenity HTML reporting capability.

4.They can be viewed under ->/target/site/serenity/index.html

**In CI/CD**

1.Project has to be hosted in repository

2.CI/CD setup in yaml file has been done, so it can be started directly

3.Simpy start the pipeline and can see artifacts to be downloading later execution starts.

4.Once the execution is completed, resulst can be seen with Build Pass/Fail.

5.It has beeen designed if atleast 1 test scenario fails then Build fails else Build will be passed, as in CI/CD build fail represents test failures and it has to be fixed before deploying to other regions.

6.After run, project artificats including report will be available in project Gitlab Test-Job dashboard


**Execution Results:**


1.There are total 11 Scenarios which constitues of 36 Test cases.

2.Out of which 34 test cases are passed and 2 test cases are failed that can be clearly visible in the report

3.2 Test cases were failed because of the fact that when user makes a search product GET call for products orange and pasta there are results in Response which are not relavant to the product searched.Thats the reason the Test has been failed and so as the Build which is Expected result.


**Results:**

[![Results](https://gitlab.com/adityapulluru/leaseplan-productSearchApi/-/blob/main/Serenity_results.PNG "Serenity Results")]

[![Serenity_Report](https://gitlab.com/adityapulluru/leaseplan-productSearchApi/-/blob/main/Serenity_Report.PNG "Report")]

[![Failure_Reason](https://gitlab.com/adityapulluru/leaseplan-productSearchApi/-/blob/main/Failure_Reason.PNG "Failure_Reason")]


**Refactored:**

Below Folders has been modified

1.Gradle folder->Removed as Maven is used as Build tool

2.History folder->>Removed as it is Not used

3.build.gradle->Removed as Maven is used as Build tool

4.gradlew->Removed as Maven is used as Build tool

5.gradlebat->Removed as Maven is used as Build tool

6.Serinity properties->Removed as serenity.config file is available


**Other Important points:**

.Reporting: Serenity BDD Report.

.Test Report: Test Report is saved in location - "target/site/serenity/index.html", (Kindly refresh after mvn clean verify)

.Coverage: Only Scenarios related to GET method for provided End point has been covered, as other methods are not implemented in the API.


For any queries, please Email me at adityapulluru@gmail.com
